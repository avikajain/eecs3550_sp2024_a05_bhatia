# EECS3550_SP2024_A05_BHATIA


## Getting started

Welcome to our modified version of the Yahtzee game. This project is the demo product built by a three-memebers teams including: Avika Jain, Jayant Bhatia, and Tran Bao Ngoc Nguyen.

This README.md file is a console-based implementation of the Yahtzee game in C++. It includes game logic, automated tests, and a CMake configuration for building and running the project. In addition, it also includes our contact email for answering any of your questions.

## Welcome to our modified version of the Yahtzee game project

### Description
This project is a version control-enabled modification of the Yahtzee game. Yahtzee is a traditional dice game in which players roll five dice to create specific combinations and score points based on those combinations. Check out https://en.wikipedia.org/wiki/Yahtzee to learn more about the game and its scoring rules. Players can score in a variety of categories, including "Full House," "Small Straight," and "Yahtzee" (five of a kind). We're building a single-player version. Unlike traditional Yahtzee, where a user can keep individual dice and re-roll, in this game, a user can only accept one score each roll. So, typical gameplay would be as follows:
1. Roll the dices,
2. Select a score/category,
3. Repeat step 1 and 2 until all categories are filled,
4. Automatically calculate and display the final score.

The following guidelines are applied: 
Implementation The game must be programmed in C++. 
Class Structure: Your project should contain at least four classes: 
1. Die: This represents a single dice. 
2. Dice: Controls a series of dice. 
3. ScoreCard: Tracks the player's scores in many categories. 
4. Game: Manages the game flow and interactions. 

The main method in main.cpp will then create a Game object and use Game.play() to start the game.

## Installation

### Prerequisites
- C++ compiler (e.g., g++)
- CMake / GoogleTest (for building, any one of the two)

### CMake
Download and install [CMake](https://cmake.org/download/). During installation, ensure to add CMake to the system path for all users or the current user.

Download and install Catch2: clone the Catch2 repository to your local machine. You can do this using Git:  git clone https://github.com/catchorg/Catch2.

### Project Structure

EECS3550_SP2024_A05_BHATIA/

├── CMakeLists.txt         # Top-level CMake configuration file

├── src/                   # Source files for the game

│   ├── CMakeLists.txt     # CMake file for src

│   ├── main.cpp           # Main game logic

│   └── Game.cpp

│   └── Game.h

│   └── Die.cpp 

│   └── Die.h 

│   └── Dice.cpp 

│   └── Dice.h

│   └── ScoreCard.cpp 

│   └── ScoreCard.h

├── tests/                 # Test directory

│   ├── CMakeLists.txt     # CMake file for tests

│   └── unit_tests.cpp      # Test cases for Yahtzee game

└── bin/                   # Output directory for binaries



### Building and Running

1. Clone this repository or download it as a ZIP file and extract it.
2. Open Visual Studio.
3. Go to `File` > `Open` > `Folder...` and select the `EECS3550_SP2024_A05_BHATIA` folder.

### Building with CMake (Command Line)

Alternatively, you can build and run the project using CMake through the terminal:

```bash
mkdir build
cd build
cmake ..
cmake --build .             # Build the project
ctest                       # Run tests
```
### Running Tests

This project uses Catch2 for unit testing. Tests are located in the "tests" folder.

### Command Line

If you use the command line: ctest

This will run all tests and display the results.


## Contributing
We are looking forward to receiving your contributions. While requesting for merging your feature branches, please uncheck the option which mentions deleting the source code. By doing this, we don't loose our source code while merging your branch. This is for the ability to recover the main program for this project if some big issues happen.      

## Authors and acknowledgment
Thank you for your time reading this README.md file and playing this modified version of the Yathzee game. We wholeheartedly appreciate all of your contribution to our project. 

Looking forward to receiving your supports and contributions. 

## License
This project is licensed under Jayant Bhatia, Avika Jain, and Tran Bao Ngoc Nguyen. The ideas for this project is the Yahtzee game. Please refer to this following link to enjoy the full Yahtzee game yoursleves: https://en.wikipedia.org/wiki/Yahtzee
