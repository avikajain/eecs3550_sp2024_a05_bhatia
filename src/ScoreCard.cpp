#include "ScoreCard.h"

ScoreCard::ScoreCard(int numCategories) : scores(numCategories, -1) {}

void ScoreCard::setScore(int category, int score)
{
    scores[category] = score; // Set the score for a category
}

int ScoreCard::getScore(int category) const
{
    return scores[category]; // Get the score for a category
}

bool ScoreCard::isCategoryFilled(int category) const
{
    return scores[category] != -1; // Check if a category is filled
}

int ScoreCard::getTotalScore() const
{
    int total = 0;
    for (int score : scores)
    {
        if (score != -1)
            total += score;
    }
    return total;
}
