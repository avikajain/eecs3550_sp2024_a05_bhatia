#ifndef DIE_H
#define DIE_H

#include <cstdlib> // for rand()

class Die {
private:
    int value; // Current value of the die
public:
    Die(); // Constructor
    void roll(); // Roll the die
    int getValue() const; // Get the value of the die
    void setValues(int newValue); 
};

#endif // DIE_H
