#ifndef DICE_H
#define DICE_H

#include <vector>
#include "Die.h" 

class Dice {
private:
    std::vector<Die> dice; 

public:
    Dice(int numDice);
    void rollAll(); 
    int getValue(int index) const; 
    void setValues(const std::vector<int>& newValues);
};

#endif // DICE_H
