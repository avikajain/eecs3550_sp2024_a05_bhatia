#ifndef SCORECARD_H
#define SCORECARD_H

#include <vector>

class ScoreCard {
private:
    std::vector<int> scores; // Scores for each category
public:
    ScoreCard(int numCategories); // Constructor
    void setScore(int category, int score); // Set the score for a category
    int getScore(int category) const; // Get the score for a category
    bool isCategoryFilled(int category) const; // Check if a category is filled
    int getTotalScore() const; // Get the total score
};

#endif // SCORECARD_H
