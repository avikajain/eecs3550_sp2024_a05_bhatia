#include "Dice.h"

// Constructor
Dice::Dice(int numDice) {
    // Create and add numDice instances of Die to the vector
    for (int i = 0; i < numDice; ++i) {
        dice.push_back(Die());
    }
}

// Roll all dice
void Dice::rollAll() {
    for (auto& die : dice) {
        die.roll();
    }
}

// Get the value of a die at a specific index
int Dice::getValue(int index) const {
    return dice[index].getValue();
}

void Dice::setValues (const std::vector<int>& newValues) {
    if (newValues.size() == dice.size()) {
        for (size_t i = 0; i < newValues.size(); ++i) {
            dice[i].setValues(newValues[i]);
        }
    } else {
        // Handle error, maybe throw an exception
    }
}