#define CATCH_CONFIG_MAIN
#include <catch2/catch_test_macros.hpp>
#include "../src/Game.h"



TEST_CASE("Test Die class") {                   //Test Case added by Avika Jain
    Die die;

    SECTION("Initial value is valid") {
        REQUIRE(die.getValue() >= 1);
        REQUIRE(die.getValue() <= 6);
    }

    SECTION("Rolling changes value") {
        int prevValue = die.getValue();
        die.roll();
        REQUIRE(prevValue != die.getValue());
    }
}

TEST_CASE("Test rolling all dice", "[Dice]") {               //Test Case added by Tran Bao Ngoc Nguyen
    SECTION("Rolling 5 dice") {
        Dice dice(5); // Create a Dice object with 5 dice

        // Roll all dice
        dice.rollAll();

        // Check if all dice values are within valid range (1-6)
        for (int i = 0; i < 5; ++i) {
            int value = dice.getValue(i);
            REQUIRE(value >= 1);
            REQUIRE(value <= 6);
        }
    }
}


TEST_CASE("Test cases for calculateScore1() method") {             //Test case added by Jayant Bhatia 
    Game game; 

    SECTION("Test score calculation for Ones") {
       
        game.dice.setValues({3, 1, 5, 6, 1});
        REQUIRE(game.calculateScore(0) == 2); 
    }
} 

TEST_CASE("Test cases for calculateScore2() method") {             //Test case added by Jayant Bhatia 
    Game game; 

    SECTION("Test score calculation for Twos") {
       
        game.dice.setValues({2, 1, 5, 2, 2});
        REQUIRE(game.calculateScore(1) == 6); 
    }
} 


TEST_CASE("Test cases for calculateScore3() method") {             //Test case added by Jayant Bhatia 
    Game game; 

    SECTION("Test score calculation for Threes") {
       
        game.dice.setValues({3, 1, 5, 6, 3});
        REQUIRE(game.calculateScore(2) == 6); 
    }
} 

TEST_CASE("Test cases for calculateScore4() method") {             //Test case added by Jayant Bhatia 
    Game game; 

    SECTION("Test score calculation for Fours") {
       
        game.dice.setValues({2, 1, 5, 6, 4});
        REQUIRE(game.calculateScore(3) == 4); 
    }
} 

TEST_CASE("Test cases for calculateScore5() method") {             //Test case added by Jayant Bhatia 
    Game game; 

    SECTION("Test score calculation for Fives") {
       
        game.dice.setValues({1, 5, 4, 3, 5});
        REQUIRE(game.calculateScore(4) == 10); 
    }
} 


TEST_CASE("Test cases for calculateScore6() method") {             //Test case added by Jayant Bhatia 
    Game game; 

    SECTION("Test score calculation for Sixes") {
       
        game.dice.setValues({2, 1, 3, 6, 6});
        REQUIRE(game.calculateScore(5) == 12); 
    }
} 







TEST_CASE("Test cases for calculateScore7() method") {             //Test case added by Jayant Bhatia 
    Game game; 

    SECTION("Test score calculation for Three of a Kind") {
       
        game.dice.setValues({3, 3, 3, 1, 2});
        REQUIRE(game.calculateScore(6) == 12); 
    }
} 

TEST_CASE("Test cases for calculateScore8() method") {             //Test case added by Tran Bao Ngoc Nguyen 
    Game game; 

    SECTION("Test score calculation for Four of a Kind") {
       
        game.dice.setValues({6, 6, 6, 6, 2});
        REQUIRE(game.calculateScore(7) == 26); 
    }
} 


TEST_CASE("Test cases for calculateScore9() method") {             //Test case added by Tran Bao Ngoc Nguyen 
    Game game; 

    SECTION("Test score calculation for Full House") {
       
        
        game.dice.setValues({4, 4, 4, 6, 6});
        REQUIRE(game.calculateScore(8) == 25); 
    }
} 



TEST_CASE("Test cases for calculateScore10() method") {             //Test case added by Tran Bao Ngoc Nguyen 
    Game game; 

    SECTION("Test score calculation for Small Straight") {
       
        
        game.dice.setValues({1, 2, 3, 4, 6});
        REQUIRE(game.calculateScore(9) == 30); 
    }
} 


TEST_CASE("Test cases for calculateScore11() method") {             //Test case added by Tran Bao Ngoc Nguyen 
    Game game; 

    SECTION("Test score calculation for Large Straight") {
       
        
        game.dice.setValues({1, 2, 3, 4, 5});
        REQUIRE(game.calculateScore(10) == 40); 
    }
}  

TEST_CASE("Test cases for calculateScore12() method") {             //Test case added by Tran Bao Ngoc Nguyen 
    Game game; 

    SECTION("Test score calculation for Yahtzee") {
       
        game.dice.setValues({6, 6, 6, 6, 6});
        REQUIRE(game.calculateScore(11) == 50); 
    }
} 


TEST_CASE("Test displayFinalScore() method") {       //Test Case added by Avika Jain
    Game game;
    // Manually fill the scorecard with some values
    game.calculateScore(0); // Calculate score for Ones
    game.calculateScore(1); // Calculate score for Twos
    game.calculateScore(2); // Calculate score for Threes
    // Add more categories as needed

    // Final score should be the sum of all filled categories
    REQUIRE(game.scorecard.getTotalScore() == 0); // Update the expected total score accordingly
}


TEST_CASE("Test calculateScore() method for different categories") {     //Test Case added by Avika Jain
    Game game;
    // Test Ones category
    REQUIRE(game.calculateScore(0) == 0); // Initial score for Ones should be 0

    // Test Three of a Kind category
    REQUIRE(game.calculateScore(6) == 0); // Initial score for Three of a Kind should be 0

    }


TEST_CASE("ScoreCard class tests1", "[ScoreCard]") {               //Test Case added by Avika Jain
    SECTION("ScoreCard constructor initializes all scores to -1") {
        int numCategories = 13;
        ScoreCard scorecard(numCategories);
        for (int i = 0; i < numCategories; ++i) {
            REQUIRE(scorecard.getScore(i) == -1);
        }
    }


    SECTION("Setting and getting score for a category works correctly") {    
        int numCategories = 13;
        ScoreCard scorecard(numCategories);
        int category = 5;
        int score = 25;
        scorecard.setScore(category, score);
        REQUIRE(scorecard.getScore(category) == score);
    }
}


TEST_CASE("ScoreCard class tests2", "[ScoreCard]") {               //Test Case added by Avika Jain
    SECTION("ScoreCard constructor initializes all scores to -1") {
        int numCategories = 13;
        ScoreCard scorecard(numCategories);
        for (int i = 0; i < numCategories; ++i) {
            REQUIRE(scorecard.getScore(i) == -1);
        }
    }

    SECTION("Checking if a category is filled works correctly") {   
        int numCategories = 13;
        ScoreCard scorecard(numCategories);
        int category = 5;
        int score = 25;
        scorecard.setScore(category, score);
        REQUIRE(scorecard.isCategoryFilled(category) == true);
    }
}

TEST_CASE("ScoreCard class tests4", "[ScoreCard]") {               //Test Case added by Avika Jain
    SECTION("ScoreCard constructor initializes all scores to -1") {
        int numCategories = 13;
        ScoreCard scorecard(numCategories);
        for (int i = 0; i < numCategories; ++i) {
            REQUIRE(scorecard.getScore(i) == -1);
        }
    }

    SECTION("Invalid category") {
        Game game;
        
        // Assuming category 15 is invalid
        game.calculateScore(15); 
        // Check that the score for category 15 was not updated
        REQUIRE(game.calculateScore(15) == 0);
    }
}

TEST_CASE("Checking if a category is filled after setting a score", "[score]") {   //Test Case added by Avika Jain
    ScoreCard scorecard(13);
    scorecard.setScore(0, 10); // Set score for Ones category
    REQUIRE(scorecard.isCategoryFilled(0) == true);
}
 